import React from 'react'
import ReactDOM from 'react-dom'
import App from './app'

const appRootElement = document.createElement('div')
document.body.appendChild(appRootElement)

ReactDOM.render(<App hello="world" />, appRootElement)
