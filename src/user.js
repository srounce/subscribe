import { authResponse$ } from './auth'

export const user$ = authResponse$
  .map(({ response }) => response.data.user)
  .startWith(null)
  .shareReplay(1)
