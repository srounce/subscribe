import { Observable, Subject } from 'rxjs'
import { registerRequest$, request$$, response$ } from './net'

const { zip } = Observable

const login$ = new Subject()

export const login = (username, password) => login$.next({ username, password })

const LOGIN_REQUEST_TYPE = 'auth:login'

export const authRequest$ = login$.map(body => ({
  type: LOGIN_REQUEST_TYPE,
  data: {
    url: `${process.env.LLOOP_API_URL}/auth`,
    body,
    method: 'POST'
  }
}))

registerRequest$(authRequest$)

export const authResponse$ = request$$
  .filter(({ type }) => type === LOGIN_REQUEST_TYPE)
  .mergeMap(request =>
    response$
      .find(({ id }) => id === request.id)
      .map(response => ({ request, response }))
  )
  .share()

const accessToken$ = authResponse$
  .map(({ response }) => response.data.access_token)
  .startWith(null)

export const auth$ = zip(accessToken$, accessToken => ({
  accessToken
})).share()
