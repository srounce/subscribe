import { Observable, Subject } from 'rxjs'
import { AjaxObservable } from 'rxjs/observable/dom/AjaxObservable'
import uuid from 'uuid/v4'

const { of } = Observable

export const request$$ = new Subject()
  .mergeAll()
  .map(v => ({ id: uuid(), ...v }))
  .share()

export const response$ = request$$
  .map(({ id, type, data }) =>
    new AjaxObservable({
      crossDomain: true,
      ...data
    })
      .catch(of)
      .map(xhr => ({ id, type, data: xhr.response, xhr }))
      .catch(error => of({ id, type, error }))
  )
  .mergeAll(3)
  .catch(e => console.error(e) || of())
  .publish()

response$.connect()

export const registerRequest$ = input$ => request$$.next(input$)
