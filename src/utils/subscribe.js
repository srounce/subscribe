import React, { Component } from 'react'

import { from } from 'rxjs/observable/from'
import { of } from 'rxjs/observable/of'
import { race } from 'rxjs/observable/race'
import 'rxjs/add/operator/combineAll'
import 'rxjs/add/operator/debounceTime'

import isEqual from 'lodash.isequal'

const defaultOptions = () => ({
  overwriteProps: false,
  rate: 10
})

const prop$fromSubscriptionMap = subscriptionMap => {
  const subscriptionKeys = Object.keys(subscriptionMap)

  return from(Object.values(subscriptionMap).concat(of(null)))
    .map(s => race(s, s.startWith(null)))
    .combineAll((...latest) =>
      subscriptionKeys.reduce(
        (a, k, i) => ({
          ...a,
          [k]: latest[i]
        }),
        {}
      )
    )
}

const isObservable = maybeObservable =>
  typeof maybeObservable.subscribe === 'function'

export default (subscriptionMap, userOptions) => {
  const options = {
    ...defaultOptions(),
    ...(userOptions || {})
  }

  let prop$ = isObservable(subscriptionMap)
    ? from(subscriptionMap)
    : prop$fromSubscriptionMap(subscriptionMap)

  if (options.rate) {
    prop$ = prop$.debounceTime(options.rate)
  }

  const SubscriberProxy = SubscribedComponent => {
    class Subscriber extends Component {
      constructor (props, context) {
        super(props, context)

        this.subscribed = false
        this.subscription = null
      }

      componentWillMount () {
        this.subscription = prop$.subscribe(
          v => {
            this.subscribed = true
            this.setState(v)
          },
          e => console.error(e) // TODO: REPLACE THIS!
        )
      }

      componentWillUnmount () {
        this.subscription.unsubscribe()
        this.subscription = null
        this.subscribed = false
      }

      shouldComponentUpdate (nextProps, nextState) {
        return (
          this.subscribed &&
          (!isEqual(this.props, nextProps) || !isEqual(this.state, nextState))
        )
      }

      render () {
        // TODO: Optimize this lol
        const componentProps = options.overwriteProps
          ? { ...this.props, ...this.state }
          : { ...this.state, ...this.props }

        return <SubscribedComponent {...componentProps} />
      }
    }

    const componentName =
      SubscribedComponent.displayName || SubscribedComponent.name || '<unknown>'

    Subscriber.displayName = `Subscriber(${componentName})`

    return Subscriber
  }

  return SubscriberProxy
}
