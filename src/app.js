import React from 'react'
import { Observable, Subject, BehaviorSubject } from 'rxjs'
import titlecase from 'titlecase'
import subscribe from './utils/subscribe'
import { auth$, authResponse$, login } from './auth'
import { user$ } from './user'
import styles from './app.css'

const { of } = Observable

const loginClick$ = new Subject()
const usernameInput$ = new BehaviorSubject('admin').distinctUntilChanged()
const passwordInput$ = new BehaviorSubject('password').distinctUntilChanged()

of(usernameInput$, passwordInput$)
  .combineAll((username, password) => ({ username, password }))
  .switchMap(v => loginClick$.mapTo(v))
  .forEach(({ username, password }) => login(username, password))

const User = ({ user = {} }) => {
  const { id, username } = user || {}
  console.log(user)

  return (
    <div className={styles.user}>
      <h2>Hi, {titlecase(username)}</h2>
      <dl className={styles.tempPropView}>
        <dt>Id</dt>
        <dd>{id}</dd>
        <dt>Username</dt>
        <dd>{username}</dd>
      </dl>
    </div>
  )
}

const SubscribedUser = subscribe({
  user: user$
})(User)

const App = ({
  hello,

  user,

  isAuthenticated,
  response,
  request,
  username = '',
  password = '',

  clickLogin,
  updateUsername,
  updatePassword
}) => (
  <div>
    <h1>Lloop</h1>
    <h2>Hello: {hello}</h2>
    <dl className={styles.tempPropView}>
      <dt>Username</dt>
      <dd>
        <input
          type="text"
          value={username}
          onChange={({ target: { value } }) => updateUsername(value)}
        />
      </dd>
      <dt>Password</dt>
      <dd>
        <input
          type="password"
          value={password}
          onChange={({ target: { value } }) => updatePassword(value)}
        />
      </dd>
      <dt>Login</dt>
      <dd>
        <button onClick={() => clickLogin()}>Click me</button>
      </dd>
    </dl>
    {isAuthenticated ? <SubscribedUser /> : null}
    {response ? (
      <dl className={styles.tempPropView}>
        <dt>request</dt>
        <dd>
          <pre>{request}</pre>
        </dd>
        <dt>response</dt>
        <dd>
          <pre>{response}</pre>
        </dd>
      </dl>
    ) : null}
  </div>
)

export default subscribe({
  user: user$,
  request: authResponse$.map(({ request }) =>
    JSON.stringify(request.data, ' ', 2)
  ),
  response: authResponse$
    .map(({ response }) => response.data || response.error.stack)
    .map(result => JSON.stringify(result, ' ', 2)),
  isAuthenticated: auth$.map(({ accessToken }) => Boolean(accessToken)),
  username: usernameInput$,
  password: passwordInput$,
  updateUsername: of(v => usernameInput$.next(v)),
  updatePassword: of(v => passwordInput$.next(v)),
  clickLogin: of(v => loginClick$.next(v))
})(App)
