const path = require('path')
const crypto = require('crypto')
const jsonServer = require('json-server')
const jwt = require('jsonwebtoken')
const escapeRegExp = require('escape-regexp')

const { LLOOP_API_PORT } = process.env

const server = jsonServer.create()
const router = jsonServer.router(path.join(__dirname, 'db.json'))
server.use(
  jsonServer.defaults({
    bodyParser: true
  })
)

const authenticatedRoutes = ['users'].map(pattern => {
  if (pattern instanceof RegExp) {
    return pattern
  }

  return new RegExp(`^/${escapeRegExp(pattern)}.*`)
})

const SECRET_KEY = crypto.randomFillSync(Buffer.alloc(32)).toString('hex')
const expiresIn = '1h'
console.log(`Super secret key is: ${SECRET_KEY}`)

const getTable = tableName => router.db.getState()[tableName]

// Create a token from a payload
const createToken = payload => jwt.sign(payload, SECRET_KEY, { expiresIn })

// Verify the token
const verifyToken = token =>
  jwt.verify(
    token,
    SECRET_KEY,
    (err, decode) => (decode !== undefined ? decode : err)
  )

// Check if the user exists in database
const isAuthenticated = ({ username, password }) => {
  const user = getTable('users').find(
    user => user.username === username && user.password === password
  )

  if (!user) {
    return null
  }

  const { password: _, ...sanitizedUser } = user

  return sanitizedUser
}

server.post('/auth', (req, res) => {
  if (typeof req.body !== 'object') {
    const status = 401
    const message = 'No request body passed'
    res.status(status).json({ status, message })
    return
  }

  const { username, password } = req.body
  const user = isAuthenticated({ username, password })
  if (!user) {
    const status = 401
    const message = 'Incorrect username or password'
    res.status(status).json({ status, message })
    return
  }
  const access_token = createToken({ username, password })
  res.status(200).json({ access_token, user })
})

server.use(/^(?!\/auth).*/, (req, res, next) => {
  const uriPath = req.baseUrl + req.path

  if (authenticatedRoutes.filter(r => r.test(uriPath)).length === 0) {
    next()
    return
  }

  const hasBearerToken =
    req.headers.authorization === undefined ||
    req.headers.authorization.split(' ')[0] !== 'Bearer'

  if (hasBearerToken) {
    const status = 401
    const message = 'Error in authorization format'
    res.status(status).json({ status, message })
    return
  }
  try {
    verifyToken(req.headers.authorization.split(' ')[1])
    next()
  } catch (err) {
    const status = 401
    const message = 'Error access_token is revoked'
    res.status(status).json({ status, message })
  }
})

const port = LLOOP_API_PORT || 3000

server.use(router)
server.listen(port, () => {
  console.info(`Mock server is running on port ${port}`)
})
